 **Mobile oriented calculator made in Javascript**
- Webpack used as Module Bundler and Config library
- BOOTSTRAP included for it's design system
- Under the component-based and dynamic html rendering principles of REACT's Library.
- Developer: Jesus Beltran.
- By the time(V1.0) only supports languages where the decimal separators are dots and the thousands separators are commas, so please
  change the system language on the device you will use the app to english.