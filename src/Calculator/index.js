import React from 'react';
import Keyboard from "../Keyboard/index.js"
import Screen from "../Screen/index.js"
import './style.scss';

export default class Calculator extends React.Component {
  constructor(props){
    super(props);
    this.state={current:0,irt:[], history:[""],operation: {input:"",position:0,index:0}}
   
  }

  handleDeleteAll = () =>{
  
   this.setState({current:this.state.current,irt: 0,operation:{input:"",position:0, index:0}})
    
  }
  handleEqual = () =>{
    
    let input = this.state.operation.input, evaluation, error = false
    try {
      evaluation= eval(input); 
     } catch (e) {
       if (e instanceof SyntaxError || e instanceof TypeError) {
        error = true
       }
       
     }
     if(!error){
      evaluation = evaluation.toString()
      let hist=[...this.state.history,input];
      this.setState({irt:evaluation,history:hist,current:this.state.history.length+1,operation:{...this.state.operation,input:evaluation,position:0,index:0}})
      console.log(this.state.history)

     }
  }
  handleWrite=(sym)=>{
    let input = this.state.operation.input, evaluation,count=0, position =this.state.operation.index
    console.log(isNaN(input.charAt(position-1)),isNaN(sym),input.charAt(position))
    if((sym!="()"&&sym!=".") && (input.charAt(position-1)!="(" && input.charAt(position-1)!= ")" && input.charAt(position-1)!="."&& input.charAt(position+1)!= ")"&& input.charAt(position+1)!= "("&& input.charAt(position+1)!= ".")&&isNaN(sym)&&(isNaN(input.charAt(position-1)))){
      console.log(input.charAt(position+2))
      input = input.substring(0,position-1)+sym+input.substring(position+1, input.length)
      
     count=1
    }
    
    else{
      console.log(this.state.operation.index+1,this.state.operation.index+1)
      input = input.substring(0,this.state.operation.index+1)+sym+input.substring(this.state.operation.index+1, input.length)
    }
      try {
       evaluation= eval(input); 
      } catch (e) {
        if (e instanceof SyntaxError  ) {
          evaluation=this.state.history[this.state.current-1];
        }
      }
         this.setState({irt:evaluation,operation:{...this.state.operation,input:input,position:this.state.operation.position+1-count,index:this.state.operation.index+1-count }}) 
    
  }
  handleDelete(){
    let input = this.state.operation.input, evaluation
    input = input.substring(0,(this.state.operation.index==input.length)?this.state.operation.index-1:this.state.operation.index)+input.substring(this.state.operation.index+1, input.length)
    
   
    try {
      evaluation= eval(input); 
     } catch (e) {
       if (e instanceof SyntaxError || e instanceof TypeError) {
        evaluation=this.state.history[this.state.current-1];
       }
     }
    this.setState({irt:evaluation,operation:{...this.state.operation,input:input,position:this.state.operation.position-1,index:this.state.operation.index-1 }}) 
    
   
  }
  render() {
    this.props.myState(this);
    return (

      <div className="container">
        
        <Screen irt={this.state.irt} getDisplay={this.props.getDisplay} actual={this.state.operation.input} pos={this.state.operation.position}/>
        <Keyboard  equal={this.handleEqual} deleteAll ={this.handleDeleteAll} write={this.handleWrite} delete={()=>{this.handleDelete()}}index= {this.state.operation.index} />
      </div>

    )
  }
}