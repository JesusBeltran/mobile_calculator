import React from 'react';

import './style.scss';
export default class Keyboard extends React.Component{
    constructor(props){
        super(props)
        
    }
    
      render() {
        return (<div className="sec-2">
            <button onClick={this.props.deleteAll} className="button "><i className="fab blue fa-react fa-lg "></i></button>
            <button onClick={()=>{this.props.write("()")}}className="button  blue ">( )</button>
            <button onClick={this.props.delete} className="button "> <i className="fas blue fa-arrow-left fa-sm "></i></button>
            <button onClick={()=>{this.props.write("/")}}className="button  blue" >	÷ 	</button>
            <button onClick={()=>{this.props.write("7")}}  className="button ">7</button>
            <button onClick={()=>{this.props.write("8")}}  className="button ">8</button>
            <button onClick={()=>{this.props.write("9")}}  className="button ">9</button>
            <button onClick={()=>{this.props.write("*")}}className="button blue ">x</button>
            <button onClick={()=>{this.props.write("4")}}  className="button ">4</button>
            <button onClick={()=>{this.props.write("5")}}  className="button ">5</button>
            <button onClick={()=>{this.props.write("6")}}  className="button ">6</button>
            <button onClick={()=>{this.props.write("+")}}className="button blue ">+</button>
            <button onClick={()=>{this.props.write("1")}}  className="button ">1</button>
            <button onClick={()=>{this.props.write("2")}} className="button ">2</button>
            <button onClick={()=>{this.props.write("3")}}  className="button ">3</button>
            <button onClick={()=>{this.props.write("-")}}className="button blue ">-</button>
            <button onClick= {()=>{this.props.write("0")}}  className="button ">0</button>
            <button onClick={()=>{this.props.write(".")}}className="button ">.</button>
            <button onClick={this.props.equal} className="large_button button blue">=</button>
            <button className="button"> </button>
            <button className="button"></button>
            <button className="button"></button>
            <button className="button"></button>
            </div>
        )
      }
}